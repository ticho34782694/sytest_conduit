SyTest Conduit Plugin
----------------------------------------------------------------

This is a fork of the original https://github.com/valkum/sytest_conduit which is tailored to work with conduit:next branch and in a Docker container.

This plugin for [SyTest](https://github.com/matrix-org/sytest) add a homeserver implementation for [Conduit](https://conduit.rs)

The intended usage is:

1. Clone the necessary Git repositories:
    - conduit (https://gitlab.com/famedly/conduit/)
    - sytest (https://github.com/matrix-org/sytest)
    - sytest_conduit (this repository)
2. Build the docker image for sytest_conduit:
```
docker build -t sytest_conduit:20220204 -f docker/Dockerfile .
```
(use whatever local image name/tag you like)

3. Run Sytest:
```
IMAGE="sytest_conduit:20220204"

CONDUIT_SRC="/path/to/conduit/repo/clone"
SYTEST_SRC="/path/to/sytest/repo/clone"
SYTEST_CONDUIT_SRC="/path/to/sytest_conduit/repo/clone"
LOG_DIR="/path/to/directory/for/test/output"
BUILD_TARGET_DIR="${LOG_DIR}/target"

mkdir -p "${LOG_DIR}"
mkdir -p "${CONDUIT_SRC}"
docker run \
  --rm \
  -it \
  -v "${CONDUIT_SRC}":/src:rw \
  -v "${SYTEST_SRC}":/sytest:ro \
  -v "${SYTEST_CONDUIT_SRC}":/sytest/plugins/sytest-conduit:ro \
  -v "${LOG_DIR}":/logs:rw \
  -v "${BUILD_TARGET_DIR}":/src/target:rw \
  $IMAGE
```

When running this often, it is a good idea to also save the cargo build cache, so the conduit rebuild takes less time. This can be done by adding another volume to the container (path to the `dotcargoregistry` directory can of course be anywhere you like):

```
  -v "${LOG_DIR}/dotcargoregistry":/root/.cargo/registry:rw \
```
